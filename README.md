# README #

MakeColors is a helper tool to generate a colors.txt file out of the nodes.txt exported by dumpnodes and the texture files.

### Usage ###

```

Usage: MakeColors.exe [options] search directories
generates a MinetestMapper colors.txt file out of a nodes.txt and texture files

Options:
  -?, -h, --help             Displays this help.
  -v, --version              Displays version information.
  -i, --input <nodes.txt>    path to <nodes.txt>
  -o, --output <colors.txt>  path to <colors.txt>

Arguments:
  search directories         Texture search directories where the textures can
                             be found. path/to/minetest_game and path/to/mods
                             recommended
```


### Build Steps ###

qmake MakeColors.pro

make