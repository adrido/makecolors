QT += core gui

CONFIG += c++11

TARGET = MakeColors
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    makecolors.cpp

HEADERS += \
    makecolors.h
