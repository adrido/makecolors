#include <QCoreApplication>
#include <QCommandLineParser>
#include <QDebug>
#include "makecolors.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QCoreApplication::setApplicationName("Colors Txt creator");
    QCoreApplication::setApplicationVersion("0.1");

    QCommandLineParser parser;
    parser.setApplicationDescription("generates a MinetestMapper colors.txt file out of a nodes.txt and texure files");
    parser.addHelpOption();
    parser.addVersionOption();

    QCommandLineOption inputOption(QStringList() << "i" << "input",
                                   "path to <nodes.txt>",
                                   "nodes.txt");
    parser.addOption(inputOption);

    QCommandLineOption outputOption(QStringList() << "o" << "output",
                                    "path to <colors.txt>",
                                    "colors.txt",
                                    "colors.txt");
    parser.addOption(outputOption);

    parser.addPositionalArgument("search directories", "Texture search directories where the textures can be found. path/to/minetest_game and path/to/mods recommended");

    parser.process(a.arguments());

    const QStringList searchDirs = parser.positionalArguments();
    if (searchDirs.length() < 1)
        parser.showHelp(EXIT_FAILURE);

    const QString nodesTxt = parser.value(inputOption);
    const QString colorsTxt = parser.value(outputOption);


    //Read all required texture names out of the nodes.txt file

    MakeColors *cThread= new MakeColors(nodesTxt, colorsTxt, searchDirs, qApp);
    QObject::connect(cThread, SIGNAL(finished()), qApp, SLOT(quit()));
    cThread->startProcess();
    return a.exec();
}
